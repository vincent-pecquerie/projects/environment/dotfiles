#!/bin/zsh

# Colorize
export ZSH_COLORIZE_TOOL=chroma
export ZSH_COLORIZE_STYLE="colorful"
export ZSH_COLORIZE_CHROMA_FORMATTER=terminal256

# Command auto-correction.
export ENABLE_CORRECTION="true"

# Command execution time stamp shown in the history command output.
export HIST_STAMPS="mm/dd/yyyy"
export TERM="xterm-256color"
export LANG="fr_FR.UTF-8"
export LC_ALL="fr_FR.UTF-8"
export PUID=`id -u`
export PGID=`id -g`
export TZ="Europe/Paris"

export PAGER=less
export VISUAL=vim
export EDITOR='vim'
export LESS_TERMCAP_md=$'\e[1;36m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[1;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

export ZSH="$HOME/.oh-my-zsh"
export GPG_TTY=$(tty)
export RANGER_LOAD_DEFAULT_RC=FALSE

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"

export COMPOSER_MEMORY_LIMIT=-1

# Themes
export ZSH_THEME="powerlevel10k/powerlevel10k"

# Add bin folders to path
[ -d "$HOME/bin" ] && export PATH="$HOME/bin:$PATH"
[ -d "/usr/local/sbin" ] && export PATH="/usr/local/sbin:$PATH"
[ -d "$HOME/.local/bin" ] && export PATH=$HOME/.local/bin:$PATH
[ -d "$HOME/.node_modules/bin" ] && export PATH="$HOME/.node_modules/bin:$PATH"
[ -d "$HOME/go/bin" ] && export PATH="$HOME/go/bin:$PATH"
[ -d "$HOME/.dotnet/tools" ] && export PATH="$HOME/.dotnet/tools:$PATH"
[ -d "$HOME/.jenv/bin" ] && export PATH="$HOME/.jenv/bin:$PATH" && eval "$(jenv init -)"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
